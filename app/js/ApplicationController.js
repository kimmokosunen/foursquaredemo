module.exports = function(app) {
    app.controller('ApplicationController', function ($scope, $location, $route, $translate) {
        $scope.lang = 'fin';

	    $scope.closeError = function() {
	    	$scope.showerrormessage = false;
	    };

	    $scope.$on('ERROR', function(event, response) {
	    	$scope.showerrormessage = true;
	    	$scope.errormessage = response;
	    });

	    $scope.$on('SUCCESS', function(event, response) {
	    	$scope.showerrormessage = false;
	    	$scope.errormessage = '';
	    });
    });
};