module.exports = function(app) {
    app.factory('LocationService', function($geolocation, $q, $rootScope){
        return {
            currentlocation: function () {
                var deferred  = $q.defer();
                
                $geolocation.getCurrentPosition({
                    timeout: 10000
                }).then(function(position) {
                   deferred.resolve(position);
                    $rootScope.$broadcast('SUCCESS', '');
                }, function(error) {
                    $rootScope.$broadcast('ERROR', 'Your location not resolved. ' + JSON.stringify(error));
                });
                return deferred.promise;
            }
        }
    });
}