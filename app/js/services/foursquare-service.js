
module.exports = function(app) {
    app.factory('FoursquareService', function($http, $q, $rootScope) {
        var foursquare_client_id = 'CYEMKOM4OLTP5PHMOFVUJJAMWT5CH5G1JBCYREATW21XLLSZ';
        var foursquare_client_secret = 'GYNP4URASNYRNRGXR5UEN2TGTKJHXY5FGSAXTIHXEUG1GYM2';
        var foursquare_api_url = 'https://api.foursquare.com/v2/venues/search?client_id=' + foursquare_client_id + '&client_secret=' + foursquare_client_secret + '&v=20130815&query={searchstring}&ll={latitude},{longitude}';

        return {
            searchlocation: function(searchstring, latitude, longitude) {
                var deferred = $q.defer();
                $http.get(foursquare_api_url.format({searchstring: searchstring, latitude: latitude, longitude: longitude})).
                    success(function(data) {
                        deferred.resolve(data.response);
                        $rootScope.$broadcast('SUCCESS', '');
                }).error(function(error) {
                    $rootScope.$broadcast('ERROR', 'Foursquare API call failed with error:' + error);
                });
                return deferred.promise;
            }
        };
       });
};

