module.exports = function(app) {
    app.config(function ($routeProvider, $locationProvider) {

        $locationProvider.html5Mode({enabled: false, requireBase: false});

        $routeProvider.when('/foursquare', {
            templateUrl: 'foursquare/foursquare.html',
            controller: 'FoursquareController'
        });

        $routeProvider.otherwise({redirectTo: '/foursquare'});
    });
};
