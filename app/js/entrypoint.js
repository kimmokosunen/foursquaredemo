
(function() {
    'use strict';

    require('angular');
    require('angular-spinner');
    require('angular-animate');
    require('angular-route');
    require('angular-translate');
    require('angular-resource');
    require('ngGeolocation');
    require('spin.js');
    require('underscore');
    require('ngmap');

    var app = angular.module('app', [
        "ngResource",
        "ngRoute",
        "ngAnimate",
        "ngGeolocation",
        "mm.foundation",
        "angularSpinner",
        "ngMap",
        "pascalprecht.translate"]);

    app.run(function($rootScope) {
        // adds some basic utilities to the $rootScope for debugging purposes
        $rootScope.log = function(thing) {
            console.log(thing);
        };

        $rootScope.alert = function(thing) {
            alert(thing);
        };
        if (!String.prototype.format) {
            String.prototype.format = function(placeholders) {
                var s = this;
                for(var propertyName in placeholders) {
                    var re = new RegExp('{' + propertyName + '}', 'gm');
                    s = s.replace(re, placeholders[propertyName]);
                }    
                return s;
            };
        }
    }).config(function ($translateProvider) {
        $translateProvider.translations('eng', {
            TITLE: "Foursquare Demo Kimmo Kosunen"
        });
        $translateProvider.translations('fin', {
            TITLE: "Foursquare Demo Kimmo Kosunen"
        });

        $translateProvider.preferredLanguage('fin');
    });

    // templates
    require('template_cache');

    // views
    require('views/foursquare/foursquare_controller')(app);

    // services
    require('services/location-service')(app);
    require('services/foursquare-service')(app);

    // application controller
    require('ApplicationController')(app);

    // route
    require('router')(app);
}());
