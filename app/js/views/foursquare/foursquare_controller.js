module.exports = function(app) {
    app.controller('FoursquareController', function ($scope, $window, FoursquareService, LocationService) {
    var foursquareInfos = require('views/foursquare/foursquareinfos')();
    $scope.searchstring = '';

    $scope.updateView = function() {
        $scope.venues = foursquareInfos.venues;        
    };

    var updateVenues = function() {
      LocationService.currentlocation().then(function(currentlocation) {
          $scope.map.setCenter({lat: currentlocation.coords.latitude, lng: currentlocation.coords.longitude});
          FoursquareService.searchlocation($scope.searchstring, currentlocation.coords.latitude, currentlocation.coords.longitude).then(function(response) {
                foursquareInfos.updateModel(response.venues);
                $scope.updateView();
              });
          });
    };

    var centerMap = function() {
      LocationService.currentlocation().then(function(currentlocation) {
        $scope.map.setCenter({lat: currentlocation.coords.latitude, lng: currentlocation.coords.longitude});
      });
    };

    $scope.$on('mapInitialized', function(event, map) {
      centerMap();
    });
    
    $scope.searchstringchanged = function() {
        updateVenues();
    };

    $scope.refresh = function() {
      updateVenues();
    }

    updateVenues();
  });
}