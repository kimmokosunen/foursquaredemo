module.exports = function($q) {
    var venues = [];
    
    var updateModel = function(data) {
        this.venues = JSON.parse(JSON.stringify(data));
        
        this.venues.sort(function (a,b) {
           return a.location.distance-b.location.distance; 
        });
    };
    
    return {
        updateModel: updateModel,
        venues: venues
    }
}