'use strict';

var gulp = require('gulp-help')(require('gulp'));

//var express = require('express');
var runSequence = require('run-sequence');
var webserver = require('gulp-webserver');
var browserify = require('browserify');
var watchify = require('watchify');
var testem = require('testem');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var htmlmin = require('gulp-htmlmin');
var streamqueue = require('streamqueue');
var less = require('gulp-less');
var concat = require('gulp-concat');
var source = require('vinyl-source-stream');
var minifyCSS = require('gulp-minify-css');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var clean = require('gulp-clean');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var map = require('map-stream');
var es = require('event-stream');
// configs

var configs = {
    run: {
        build: false,
        debug: true,
        path: './run/'
    },
    dist: {
        build: true,
        debug: false,
        path: './dist/'
    }
}

var currentconfig;
var testem;

var paths = {
    gulp: './.gulp/'
};

var ports = {
    webserver: 8080
};

// main tasks

gulp.task('run','builds run environment and starts servers' ,function() {
    currentconfig = configs.run;
    runSequence('clean', ['lint','concat-css', 'prepare-html', 'browserify', 'copy-content', 'start-servers', 'package-app', 'watch']);
});

gulp.task('build', 'builds distribution files', function() {
    currentconfig = configs.dist;
    runSequence('clean',['lint','concat-css', 'prepare-html', 'browserify', 'package-app', 'copy-content']);
});

gulp.task('spec', 'runs tests', function() {
    currentconfig = configs.run;
    runSequence('clean', ['concat-css', 'prepare-html', 'browserify', 'copy-content', 'package-app', 'test']);
});

// sub tasks

gulp.task('test', false, function() {
    var options = {
        framework: 'jasmine',
        src_files: ['bower_components/angular/angular.js','bower_components/angular-mocks/angular-mocks.js', 'vendor/js/*.js', 'run/**/*.js', 'spec/**/*.js'],
        launch_in_dev: ['Chrome'],
        port: 7657
    };

    testem = new testem();
    testem.startDev(options);
});

gulp.task('clean', false, function() {
    if( currentconfig.build) {
        gulp.src('./c9/' + currentconfig.path + '*', {read: false}).pipe(clean({force: true}));
    }
    return gulp.src(currentconfig.path + '*', {read: false}).pipe(clean({force: true}));
});

gulp.task('watch', false, function() {
    gulp.watch('./app/js/**/*.html', ['prepare-html']);
    gulp.watch('./.gulp/js/bundle.js', ['package-app', 'copy-index']);
    gulp.watch('./app/pages/index.html', ['copy-content']);
    gulp.watch('./app/img/**/*', ['copy-content']);
    gulp.watch('./app/css/**/*', ['concat-css', 'copy-index']);
});

gulp.task('copy-content', false, function() {
    var src = [
        'app/img*/**/*',
        'app/static/**/*',
        'vendor/js*/**/*',
        'vendor/css*/**/*',
        'app/pages/**/*'// start page. last * important!!
    ];
    gulp
        .src(src)
        .pipe(gulp.dest(currentconfig.path));
});

gulp.task('copy-index', false, function() {
    var src = [
        'app/pages/**/*'     // start page. last * important!!
    ];
    gulp
        .src(src)
        .pipe(gulp.dest(currentconfig.path))
});

gulp.task('start-servers', false ,function() {
    var path = currentconfig.path;
    var options = {
        port: ports.webserver,
        host: '0.0.0.0',
        livereload: true
    };
    console.log('Serving up as webserver: ' +  path);

    gulp
        .src(path)
        .pipe(webserver(options));
});

gulp.task('concat-css', false, function() {
    streamqueue({objectMode: true},
        gulp.src('app/css/**/*.css'),
        gulp.src('vendor/css/**/*.css'),
        gulp.src('app/css/**/*.less').pipe(less())
    ).pipe(concat('app.css'))
     .pipe(gulpif(currentconfig.build, minifyCSS({keepBreaks:true})))
     .pipe(gulp.dest(currentconfig.path + 'css/'));
});

gulp.task('prepare-html', false, function() {
    gulp.src(['app/js/views/**/*.html'])
        .pipe(htmlmin({
            collapseWhiteSpace: true
        }))
        .pipe(templateCache('template_cache.js', {
            module: 'app',
            standalone: false,
            moduleSystem: 'browserify'
        }))
        .pipe(gulp.dest(paths.gulp + 'js/'))
});

gulp.task('browserify', false, function() {
    var src = 'entrypoint.js';
    var b = browserify(src, {
        debug: currentconfig.debug,
        paths: ['./app/js/', paths.gulp + 'js/']
    });
    if( !currentconfig.build) {
        console.log('Watchify connected to browserify');
        b = watchify(b);
        b.on('update', function() {
            gulp.start('lint');
            bundle(b);
        });
    }
    bundle(b);
});

gulp.task('lint', false, function () {
    gulp.src(['gruntfile.js', './app/js/**.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

function bundle(b) {
    b.bundle()
        .on('error', function(error) {
            console.log('Browserify failed!!! \n' + error);
        })
        .on('success', function() {
            console.log('Browserify done succesfully');
        })
        .pipe(source('bundle.js'))
        .pipe(ngAnnotate())
        .pipe(gulpif(currentconfig.build, buffer()))
        .pipe(gulpif(currentconfig.build, uglify()))
        .pipe(gulp.dest(paths.gulp + 'js/'))
}

gulp.task('package-app', false, function() {
    gulp.src(paths.gulp + 'js/' + 'bundle.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest(currentconfig.path + 'js/'));
});
