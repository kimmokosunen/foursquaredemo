describe("service: FoursquareService", function () {

    beforeEach(function () {
        module("app", 'app');
    });

    beforeEach(inject(function ($rootScope, $http, $q, $httpBackend, FoursquareService) {
        this.$rootScope = $rootScope;
        this.$httpBackend = $httpBackend;
        this.FoursquareService = FoursquareService;
    }));

    describe('Foursquare venues service', function () {
        it("it should search venues from foursquare api", function () {
            this.$httpBackend.expectGET('https://api.foursquare.com/v2/venues/search?client_id=CYEMKOM4OLTP5PHMOFVUJJAMWT5CH5G1JBCYREATW21XLLSZ&client_secret=GYNP4URASNYRNRGXR5UEN2TGTKJHXY5FGSAXTIHXEUG1GYM2&v=20130815&query=hae tätä&ll=123456,456787').respond({venues: 'Testidataa'});
            this.FoursquareService.searchlocation('hae tätä', '123456', '456787');
            this.$httpBackend.flush();
        });
    });
});