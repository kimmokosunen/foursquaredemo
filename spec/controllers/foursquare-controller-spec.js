describe("controller: FoursquareController", function () {

    beforeEach(function () {
        module("app", 'app');
    });

    beforeEach(function () {

        module(function( $provide) {
            $provide.value('FoursquareService', {
                  searchlocation: function() {
                    return { 
                        then: function(callback) {return callback({venues: [{name: 'Testipaikka1', location: {distance: 40}}, {name: 'Testipaikka2', location: {distance: 20}}, {name: 'Testipaikka3', location: {distance: 80}}]});}
                    };
                  }
            });

            $provide.value('LocationService', {
                  currentlocation: function() {
                    return { 
                        then: function(callback) {return callback({coords: {latitude: 54, longitude: 77}});}
                    };
                  }
            });
        });
    });

    beforeEach(inject(function($controller, $rootScope, $httpBackend) {
        this.scope = $rootScope.$new();
        this.scope.map = { setCenter: function(){}};
        this.mapMock = spyOn(this.scope.map, 'setCenter');
        $controller('FoursquareController', {
            $scope: this.scope
        });
    }));

    describe('Foursquare venues searched', function () {
        it("it should search venues when search text changes", function () {
            this.scope.searchstringchanged();
            expect(this.scope.venues).toEqual([{name: 'Testipaikka2', location: { distance: 20 } }, { name: 'Testipaikka1', location: { distance: 40 } }, { name: 'Testipaikka3', location: { distance: 80 } }]);
            expect(this.mapMock).toHaveBeenCalledWith({ lat : 54, lng : 77 });
        });
    });
});
